const express = require('express');
const gameRouter = require('../routes/GameRouter');
const playerRouter = require('../routes/PlayerRouter');

router = express.Router();

homeController = require('../controllers/HomeController');

router.get('/', homeController.home);

router
  .use('/games', gameRouter)
  .use('/players', playerRouter)


module.exports = router;