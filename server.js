const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const session = require('express-session');

const app = require('express')();
const config = require('./config');

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(session({
  secret: config.SECRET_KEY,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}));

app.all('/', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

var httpServer = http.createServer(app);

httpServer.listen(config.PORT, function () {
  console.log(`Listening on port ${config.PORT}`)
});

let io = require('socket.io')(httpServer);

io.on('connection', (socket) => {

  // create game

  socket.on('createGame', (gameId) => {
    socket.join('game'+gameId);
  });

  // refresh games-list page

  socket.on('refreshListGames', (msg) => {
    io.emit('refresh', msg);
  });
 
  // user join one game created

  socket.on('startGame', (gameId) => {

    // add client to room of game

    socket.join('game'+gameId);

    let msg = 'Coucou from back ' + gameId;

    io.to('game'+gameId).emit('message', msg);

  });

  // change turn

  socket.on('changeTurnFromFront', (gameId, playerId, checker) => {
    console.log('Game id : ' + gameId + ' --- Player id : ' + playerId);
    io.to('game'+gameId).emit('changeTurnFromBack', {playerId: playerId, checker: checker});
  });


});

let routes =  require('./routes/routes');

app.use(routes);


