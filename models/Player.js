'use strict';

module.exports = (sequelize, DataTypes) => {
	const Player = sequelize.define('Player', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		pseudo: DataTypes.STRING,
		mines: DataTypes.STRING,
		coups: DataTypes.STRING,
		game_id: DataTypes.INTEGER,
	});
	Player.associate = function(models) {
		models.Player.belongsTo(models.Game, { foreignKey: 'game_id', targetKey: 'id'});
	};

	Player.sync();

	return Player;
};
