const models = require('../models');

function index(req, res) {
    try {
        models.Player.findAll({ 
            include : {
                model:models.Game
            }
        }).then(
            player => res.send(player)
        );
    }
    catch (error) {
        console.error(error);
    }
}

function create(req, res) {
    try {

        models.Player.create(req.body).then(
            player => res.send(player)
        );
        
    }
    catch (error) {
        console.error(error);
    }
}

function show(req, res) {
    try {
        const id = req.params.id;
        models.Player.findOne(
            { where: { id: id }, 
            include : {
                model:models.Game
            } 
        }).then((game) => {
            res.send(game);
        });
    }
    catch (error) {
        console.error(error);
    }
}

function update(req, res) {
    try {
        const id = req.params.id;
        models.Player.findOne({ where: { id: id } }).then((player) => {
            player.update(req.body);
            res.send(player);
        });
    }
    catch (error) {
        console.error(error);
    }
}

function destroy(req, res) {
    try {
        const id = req.params.id;
        models.Player.findOne({ where: { id: id } }).then((player) => {
            player.destroy();
            res.send('Player ' + player.id + ' is destroy');
        });
    }
    catch (error) {
        console.error(error);
    }
}

module.exports = {
    index: index,
    create: create,
    show: show,
    update: update,
    destroy: destroy,
}
